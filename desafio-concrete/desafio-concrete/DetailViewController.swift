//
//  DetailViewController.swift
//  desafio-concrete
//
//  Created by Wellysson Avelar on 08/02/2018.
//  Copyright © 2018 Wellysson Avelar. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class DetailViewController: UITableViewController {
    var pullRequests = [PullRequest]()
    
    var login: String?
    var repository: String?
    var opened: Int = 0
    var closed: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let title = self.repository {
            self.title = title
        }
        
        configure()
    }
    
    func configure(){
        if let login = self.login, let repository = self.repository {
            view.showBlurLoader()
            GitHubService.getPullRequests(login: login, repository: repository) { (pulls) in
                self.view.removeBluerLoader()
                self.pullRequests = pulls
                self.tableView.reloadData()
                for p in pulls {
                    if p.state?.lowercased() == "open" {
                        self.opened += 1
                    }
                }
                
                self.closed = pulls.count - self.opened
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    var detailItem: NSDate? {
        didSet {
            
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pullRequests.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PullRequestTableViewCell
        
        let pull = pullRequests[indexPath.item]
        
        if let u = pull.user {
            GitHubService.getUser(login: (u.login)!, completion: { (user) in
                DispatchQueue.main.async {
                    cell.nameLabel.text = user.name
                }                
            })
        }
        
        if let urlImage = pull.user?.avatarUrl {
            
            let urlRequest = URLRequest(url: URL(string: urlImage)!)
            if let image = MyVariables.imageCache.image(for: urlRequest, withIdentifier: urlImage) {
                cell.photoImage.image = image
            }
            else {
                Alamofire.request(urlImage).responseImage(completionHandler: { (response) in
                    if let image = response.result.value {
                        MyVariables.imageCache.add(image, for: urlRequest, withIdentifier: urlImage)
                        
                        DispatchQueue.main.async {
                            if let url = URL(string: urlImage) {
                                cell.photoImage.af_setImage(withURL: url)
                            }
                        }
                    }
                })
            }
        }
        cell.setPullRequest(pullRequest: pull)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! HeaderPullRequestTableViewCell
        
        if self.pullRequests.count > 0 {
            let key1 = [NSAttributedStringKey.foregroundColor: UIColor.orange]
            let str1 = NSMutableAttributedString(string: "\(self.opened) opened", attributes: key1)
            let key2 = [NSAttributedStringKey.foregroundColor: UIColor.black]
            let str2 = NSMutableAttributedString(string: " / \(self.closed) closed", attributes: key2)
            
            let str = NSMutableAttributedString()
            str.append(str1)
            str.append(str2)
            
            cell.accountantsLabel.attributedText = str
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pull = self.pullRequests[indexPath.item]
        self.tableView.deselectRow(at: indexPath, animated: true)
        if let url = NSURL(string: pull.html_url!) as URL? {
            UIApplication.shared.openURL(url)
        }
    }


}

