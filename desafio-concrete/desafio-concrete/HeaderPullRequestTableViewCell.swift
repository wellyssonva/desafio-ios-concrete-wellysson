//
//  HeaderPullRequestTableViewCell.swift
//  desafio-concrete
//
//  Created by Wellysson Avelar on 13/02/2018.
//  Copyright © 2018 Wellysson Avelar. All rights reserved.
//

import UIKit

class HeaderPullRequestTableViewCell: UITableViewCell {

    @IBOutlet weak var accountantsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
