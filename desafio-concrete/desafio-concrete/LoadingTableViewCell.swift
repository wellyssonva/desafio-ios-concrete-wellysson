//
//  LoadingTableViewCell.swift
//  desafio-concrete
//
//  Created by Wellysson Avelar on 12/02/2018.
//  Copyright © 2018 Wellysson Avelar. All rights reserved.
//

import UIKit

class LoadingTableViewCell: UITableViewCell {

    @IBOutlet weak var loadingActivity: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.loadingActivity.startAnimating()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
