//
//  MasterViewController.swift
//  desafio-concrete
//
//  Created by Wellysson Avelar on 08/02/2018.
//  Copyright © 2018 Wellysson Avelar. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class MasterViewController: UITableViewController {

    var detailViewController: DetailViewController? = nil
    
    var repositories = [Repository]()
    var page: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configure()

        tableView.register(UINib(nibName: "LoadingTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadingCell")
    }
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    
    func configure()  {
        view.showBlurLoader()
        GitHubService.getRepositories (page: self.page) { (rep) in
            self.repositories = rep
            self.tableView.reloadData()
            self.view.removeBluerLoader()
        }
    }
    
    func nextPage() {
        GitHubService.getRepositories (page: self.page) { (rep) in
            if rep.count > 0 {
                self.page += 1
            }
            
            self.repositories += rep
            self.tableView.reloadData()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @objc
    func insertNewObject(_ sender: Any) {

        let indexPath = IndexPath(row: 0, section: 0)
        tableView.insertRows(at: [indexPath], with: .automatic)
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                let repository = self.repositories[indexPath.item]
                controller.login = repository.owner?.login
                controller.repository = repository.name
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.repositories.count + 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        

        if indexPath.item == self.repositories.count {
            if self.page > 0 {
                self.nextPage()
            }
            else {
                self.page = 1
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoadingCell", for: indexPath) as! LoadingTableViewCell
            cell.loadingActivity.startAnimating()
            return cell
            
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RepositoryTableViewCell
            let rep = repositories[indexPath.item]
            if let o = rep.owner {
                GitHubService.getUser(login: (o.login)!, completion: { (user) in
                    DispatchQueue.main.async {
                        cell.nameLabel.text = user.name
                        
                    }
                })
            }
            
            if let urlImage = rep.owner?.avatarUrl {
                let urlRequest = URLRequest(url: URL(string: urlImage)!)
                if let image = MyVariables.imageCache.image(for: urlRequest, withIdentifier: urlImage) {
                    cell.photoImage.image = image
                }
                else {
                    Alamofire.request(urlImage).responseImage(completionHandler: { (response) in
                        if let image = response.result.value {
                            MyVariables.imageCache.add(image, for: urlRequest, withIdentifier: urlImage)
                            
                            DispatchQueue.main.async {
                                if let url = URL(string: urlImage) {
                                    cell.photoImage.af_setImage(withURL: url)
                                }
                            }
                        }
                    })
                }
            }
            
            cell.setRepository(repository: rep)
            return cell
        }
    }
}

