//
//  PullRequest.swift
//  desafio-concrete
//
//  Created by Wellysson Avelar on 11/02/2018.
//  Copyright © 2018 Wellysson Avelar. All rights reserved.
//

import Foundation
import ObjectMapper
class PullRequest: Mappable {
    
    var title: String?
    var body: String?
    var user: User?
    var state: String?
    var html_url: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        self.title <- map["title"]
        self.body <- map["body"]
        self.user <- map["user"]
        self.state <- map["state"]
        self.html_url <- map["html_url"]
    }
}
