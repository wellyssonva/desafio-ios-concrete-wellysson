//
//  Repositorio.swift
//  desafio-concrete
//
//  Created by Wellysson Avelar on 08/02/2018.
//  Copyright © 2018 Wellysson Avelar. All rights reserved.
//

import Foundation
import ObjectMapper

class Repository: Mappable {
    var id: Int?
    var name: String?
    var description: String?
    var forks: Int?
    var owner: User?
    var stargazers_count: Int?
    
    required init?(map: Map) {
    }

    func mapping(map: Map) {
        self.id <- map["id"]
        self.name <- map["name"]
        self.description <- map["description"]
        self.forks <- map["forks"]
        self.stargazers_count <- map["stargazers_count"]
        self.owner <- map["owner"]
    }
}
