//
//  Usuario.swift
//  desafio-concrete
//
//  Created by Wellysson Avelar on 08/02/2018.
//  Copyright © 2018 Wellysson Avelar. All rights reserved.
//

import Foundation
import ObjectMapper

class User: Mappable {
    var name: String?
    var login: String?
    var id: Int?
    var avatarUrl: String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        self.name <- map["name"]
        self.login <- map["login"]
        self.id <- map["id"]
        self.avatarUrl <- map["avatar_url"]
    }
}
