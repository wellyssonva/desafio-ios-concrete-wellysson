
//
//  MyVariable.swift
//  desafio-concrete
//
//  Created by Wellysson Avelar on 12/02/2018.
//  Copyright © 2018 Wellysson Avelar. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage

struct MyVariables {
    static let imageCache = AutoPurgingImageCache(memoryCapacity: 50000000, preferredMemoryUsageAfterPurge: 10000000)
}
