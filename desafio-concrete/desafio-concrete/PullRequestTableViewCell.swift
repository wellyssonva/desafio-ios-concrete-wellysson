//
//  PullRequestTableViewCell.swift
//  desafio-concrete
//
//  Created by Wellysson Avelar on 11/02/2018.
//  Copyright © 2018 Wellysson Avelar. All rights reserved.
//

import UIKit

class PullRequestTableViewCell: UITableViewCell {

    @IBOutlet private weak var photoView: UIView!
    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet private weak var pullRequestNameLabel: UILabel!
    @IBOutlet private weak var pullRequestDescriptionLabel: UILabel!
    @IBOutlet private weak var usernameLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.photoView?.layer.cornerRadius = 20.0
        
        self.prepareForReuse()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()

        self.pullRequestNameLabel.text = nil
        self.pullRequestDescriptionLabel.text = nil
        self.usernameLabel.text = nil
        self.nameLabel.text = nil
        self.photoImage.image = nil
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setPullRequest(pullRequest: PullRequest) {
        if let user = pullRequest.user {
            self.usernameLabel.text = user.login
        }
        
        self.pullRequestNameLabel.text = pullRequest.title
        self.pullRequestDescriptionLabel.text = pullRequest.body
    }
}
