//
//  RepositoryTableViewCell.swift
//  desafio-concrete
//
//  Created by Wellysson Avelar on 08/02/2018.
//  Copyright © 2018 Wellysson Avelar. All rights reserved.
//

import UIKit

class RepositoryTableViewCell: UITableViewCell {
    @IBOutlet private weak var photoView: UIView!
    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet private weak var repositoryNameLabel: UILabel!
    @IBOutlet private weak var repositoryDescriptionLabel: UILabel!
    @IBOutlet private weak var forksCountLabel: UILabel!
    @IBOutlet private weak var starGazersCountLabel: UILabel!
    @IBOutlet private weak var usernameLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.photoView.layer.cornerRadius = 25.0
        prepareForReuse()
    }
    
    override func prepareForReuse() {
        self.repositoryNameLabel.text = nil
        self.repositoryDescriptionLabel.text = nil
        self.forksCountLabel.text = nil
        self.starGazersCountLabel.text = nil
        self.photoImage.image = nil
        
        self.imageView?.af_cancelImageRequest()
    }
    
    func setRepository(repository: Repository){
        self.repositoryNameLabel.text = repository.name
        self.repositoryDescriptionLabel.text = repository.description
        self.forksCountLabel.text = String(repository.forks!)
        self.starGazersCountLabel.text = String(repository.stargazers_count ?? 0)
        
        if let user = repository.owner {
            self.usernameLabel.text = user.login
        }
    }

}
