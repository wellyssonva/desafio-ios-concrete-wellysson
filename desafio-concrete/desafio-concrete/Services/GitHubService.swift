//
//  GitHubService.swift
//  desafio-concrete
//
//  Created by Wellysson Avelar on 08/02/2018.
//  Copyright © 2018 Wellysson Avelar. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class GitHubService {
    static func getRepositories(page: Int, completion: @escaping ([Repository])->()) {
        Alamofire.request("https://api.github.com/search/repositories?q=language:Java&sort=stars&page=\(page)").responseJSON { response in
            print("Request: \(String(describing: response.request))")   // original url request
            print("Response: \(String(describing: response.response))") // http url response
            print("Result: \(response.result)")                         // response serialization result
            
            if let json = response.result.value as? [String: Any] {
                if let repositories = Mapper<Repository>().mapArray(JSONObject: json["items"]) {
                    completion(repositories)
                }
                else {
                    let repositories = [Repository]()
                    completion(repositories)
                }
            }
        }
    }
    
    static func getPullRequests(login: String, repository: String, completion: @escaping ([PullRequest])->()) {
        Alamofire.request("https://api.github.com/repos/\(login)/\(repository)/pulls").responseJSON { response in
            print("Request: \(String(describing: response.request))")   // original url request
            print("Response: \(String(describing: response.response))") // http url response
            print("Result: \(response.result)")                         // response serialization result
            
            if let json = response.result.value {
                if let pullRequest = Mapper<PullRequest>().mapArray(JSONObject: json) {
                    completion(pullRequest)
                }
                else {
                    completion([PullRequest]())
                }
            }
        }
    }
    
    static func getUser(login: String, completion: @escaping (User)->()) {
        Alamofire.request("https://api.github.com/users/\(login)").responseJSON { response in
            print("Request: \(String(describing: response.request))")   // original url request
            print("Response: \(String(describing: response.response))") // http url response
            print("Result: \(response.result)")                         // response serialization result
            
            if let json = response.result.value as? [String: Any] {
                if let user = Mapper<User>().map(JSONObject: json) {
                    completion(user)
                }
            }
        }
    }
}
