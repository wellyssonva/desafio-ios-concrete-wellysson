//
//  desafio_concreteTests.swift
//  desafio-concreteTests
//
//  Created by Wellysson Avelar on 08/02/2018.
//  Copyright © 2018 Wellysson Avelar. All rights reserved.
//

import XCTest
import ObjectMapper
@testable import desafio_concrete

class desafio_concreteTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGetRepositories() {
        
        let json = JsonFiles.jsonRepository
    
        let repositories = Mapper<Repository>().mapArray(JSONObject: json["items"])
        
        XCTAssertNotNil(repositories)
        XCTAssertTrue(repositories?.count ?? 0 > 0)
    }
    
    func testGetPullRequests() {
        let json = JsonFiles.jsonPullRequest
        
        let pullRequest = Mapper<PullRequest>().map(JSONObject: json)
        
        XCTAssertNotNil(pullRequest)
    }
    
    func testGetUser() {
        let json = JsonFiles.jsonUser
        
        let user = Mapper<User>().map(JSONObject: json)
        
        XCTAssertNotNil(user)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
